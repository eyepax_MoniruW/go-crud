package entities

type Product struct{
	Id int64
	Name string
	Price int64
	Quantity int64
	Status int64
}